cmake_minimum_required(VERSION 3.0.0)
project(SoLoud VERSION 0.1.0)

#include(CTest)
#enable_testing()

add_compile_definitions(WITH_SDL2)

include_directories( include/)

add_library(SoLoud SHARED src/core/soloud.cpp src/core/soloud_audiosource.cpp src/core/soloud_bus.cpp src/core/soloud_core_3d.cpp src/core/soloud_fader.cpp)

set(CPACK_PROJECT_NAME ${PROJECT_NAME})
set(CPACK_PROJECT_VERSION ${PROJECT_VERSION})
include(CPack)
